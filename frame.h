#pragma once

#include <algorithm>
#include <iostream>
#include <vector>

#include <cassert>
#include <cstdint>
//-----------------------------------------------------------------------------

/** Internal representation of image */
class grayscale_image {
 public:
    grayscale_image(size_t x_size, size_t y_size)
            : data_(x_size * y_size, '\0')
            , x_size_(x_size)
            , y_size_(y_size) {}

    uint8_t& operator()(size_t x, size_t y) {
        return data_[x + y * x_size_];
    }
    const uint8_t& operator()(size_t x, size_t y) const {
        return data_[x + y * x_size_];
    }

    friend std::ostream& operator<<(std::ostream& os, const grayscale_image& img) {
        bool first_comma = true;
        for (const auto& px : img.data_) {
            if (!first_comma) first_comma = os << ',';
            os << px;
        }
        return os;
    }

    /// No interpolation - new size must be proportional
    void scale_down(size_t new_x, size_t new_y) {
        assert(new_x < x_size_ && "Scaling-up unsupported (dim x)");
        assert(new_y < y_size_ && "Scaling-up unsupported (dim y)");

        const auto x_scale = x_size_ / new_x;
        const auto y_scale = y_size_ / new_y;
        grayscale_image tmp_img(new_x, new_y);

        for (size_t x = 0u; x < new_x; ++x) {
            for (size_t y = 0u; y < new_y; ++y) {
                tmp_img(x, y) = subimg_median(x_scale * x,        y_scale * y,
                                              x_scale * (x + 1u), y_scale * (y + 1u));
            }
        }

        x_size_ = new_x;
        y_size_ = new_y;
        data_.swap(tmp_img.data_);
    }

 private:

    uint8_t subimg_median(size_t x1, size_t y1, size_t x2, size_t y2) const {
        assert(x1 != x2 && y1 != y2 && "Requested median of empty set");
        if (x2 > x_size_) x2 = x_size_;
        if (y2 > y_size_) y2 = y_size_;

        std::vector<uint8_t> pixels;
        pixels.reserve((x2 - x1) * (y2 - y1));

        for (auto i = x1; i < x2; ++i)
            for (auto j = y1; j < y2; ++j)
                pixels.push_back((*this)(i, j));

        std::sort(pixels.begin(), pixels.end());
        if (pixels.size() % 2)
            return pixels[pixels.size() / 2];
        return (pixels[pixels.size() / 2] + pixels[pixels.size() / 2 - 1]) / 2;
    }

    std::vector<uint8_t> data_;
    size_t x_size_, y_size_;
};
//-----------------------------------------------------------------------------

struct grayscale_frame {
    grayscale_image img;
    double ts;
};

std::ostream& operator<<(std::ostream& os, const grayscale_frame& frame) {
    return os << frame.ts << ',' << frame.img;
}
//-----------------------------------------------------------------------------
